const {createApp} = Vue;
createApp({
   data(){
        return{ 
            valorDisplay:"0",
            operador:null,
            numeroAtual:null,
            numeroAnterior:null,
            tamanhoLetra: 45 + "px" ,
        };//Fechamento return
   },//fechamen data

   methods:{
      getNumero(numero){
         this.ajusteTamanhoDisplay();         
         if(this.valorDisplay == "0"){
            this.valorDisplay = numero.toString();
         }
         else{
            if(this.operador == "=" || this.valorDisplay.includes("op")){
                  this.valorDisplay = "";
                  this.operador = null;                 
            }
            // this.valorDisplay = this.valorDisplay + numero.toString();
            //adiçao simplificada
            this.valorDisplay += numero.toString();
         }
    },//Fechamento getNumero

    limpar(){
      this.valorDisplay = "0";
      this.operador = null;
      this.numeroAnterior = null;
      this.numeroAntual = null;
      this.tamanhoLetra = 50 + "px";
    },//fechamento limpar

    decimal(){
      if(!this.valorDisplay.includes(".")){
            this.valorDisplay += ".";
      }//Fechamento if
    },//Fechamento decimal

    operacoes(operacao){
         if(this.numeroAtual != null){
            const displayAtual = parseFloat(this.valorDisplay);
            if(this.operador != null){
               switch(this.operador){
                     case "+":
                        this.valorDisplay = (this.numeroAtual + displayAtual).toString();
                        break;
                     case "-":
                        this.valorDisplay = (this.numeroAtual - displayAtual).toString();
                        break;
                     case "*":
                        this.valorDisplay = (this.numeroAtual * displayAtual).toString();
                        break;
                     case "/":
                        if(displayAtual == 0 || this.numeroAtual == 0){  
                           this.valorDisplay = "Operação impossível!";                           
                        }
                        else{
                              this.valorDisplay = (this.numeroAtual / displayAtual).toString();
                        }
                        break;

               }//Fim do switch
               
               this.numeroAnterior = this.numeroAtual;
               this.numeroAtual =  null;

               if(this.operador != "="){
                  this.operador = null;
               }

               //acertar o numero de casas decimais quando o resultado for decimasl
               if(this.valorDisplay.includes(".")){
                  const numDecimal = parseFloat(this.valorDisplay);
                  this.valorDisplay = (numDecimal.toFixed(4)).toString();

               }
            }//Fim do if
            else{
               this.numeroAnterior = displayAtual;
            }//Fim do else
         }//Fim di if numeroAtual

         this.operador = operacao;         
         this.numeroAtual = parseFloat(this.valorDisplay);
         

         if(this.operador != "="){
            this.valorDisplay = "0";
         }
         this.ajusteTamanhoDisplay();
      },//Fim das operaçoes

      ajusteTamanhoDisplay(){
         if(this.valorDisplay.length >= 31){
            this.tamanhoLetra = 14 + "px";
         }
         else if(this.valorDisplay.length >= 21)
         {
            this.tamanhoLetra = 20 + "px";
         }
         else if(this.valorDisplay.length >= 12)
         {
            this.tamanhoLetra = 30 + "px";
         }
         else{
            this.tamanhoLetra = 50 + "px";
         }
      },/*fechamento ajusteTamanhaDisplay*/
    },//Fechamento de methods

}).mount("#app");//Fechamneto app
