const {createApp} = Vue;

createApp({
        data(){
            return{
                usuario: '',
                senha: '',
                erro: null,
                sucesso: null,
            }//fechamento return
        },//Fechamento data

        methods:{
            login(){
                // alert("Testando...");

                //simulado uma requisiçao de login assincrona
                setTimeout(()=> {
                    if((this.usuario === 'Julia' && this.senha === "12345678") || 
                    
                    (this.usuario === "Rafael" && this.senha === "87654321")){
                        this.erro = null;
                        this.sucesso = "Login efetuado com sucesso!"
                        
                        // alert("Login efetuado com sucesso!");
                    }//fim do if
                    else{
                        // alert("Usuario ou senha incorretos")
                        this.erro =  "Usuário ou senha incorretos!";
                        this.sucesso = null;
                    }
                }, 1000)
            }, //fechamento login
        },//fechamanto methods

}).mount("#app");//Fechamento app